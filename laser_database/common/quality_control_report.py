# -*- coding: utf-8 -*-
"""
Created on Fri Jan 14 20:26:04 2022

@author: mateu
"""

# Python libraries
import numpy as np
import pandas as pd
from scipy.signal import find_peaks 
import datetime
from tqdm import tqdm
import itertools
import copy

# Vendor specific libraries
import idea_types.variables as var
import laser_database.process_data.utils as util
from laser_database.process_data.process_test import ProcessTest

def quality_control_report(report_path, test_operator, laser_dict, config_dict, collection_obj):
    data_dict = copy.deepcopy(util.info_dict)

    fault_dict = copy.deepcopy(util.faults_dict)

    list_data = []
    #For statement for each laser
    for laser_num, board_type in laser_dict.items():

        laser_id = copy.deepcopy(util.laser_id)

        # Unit pickle dictionary path
        hw_name = board_type + '_' + str(laser_num)
        print(f'\n\nSerial Number: {hw_name}')
        laser_path = report_path+'\\' + hw_name + '\\'

        # Open report data file
        test_data = np.load(laser_path + 'test_path_dict.pkl', allow_pickle=True)
                        
        #Variables that are unique for a laser, independent if it is a Dual or Single
        faults = []
        pwr_cons_gb = {}
        
        # Selecting Master(TX) / Slave(RX)
        #Sorted to always set order in -> First: Master; Second; Slave (only for Dual)
        #For statement for each goldbox
        for gb_name in sorted(list(test_data.keys())):
            # Goldbox Configuration
            print(f'Generating report for {gb_name.replace("_", " ")} from {hw_name.replace("_", " ")}')
            
            report_data = test_data[gb_name]
            proc_obj = ProcessTest(report_data)
            
            proc_obj.verify_tests()
            try:
                data_full = proc_obj.verify_channel()
            except:
                continue
            
            #Channel Failure variables are unique for each GB
            idx_ch_fail = np.where(data_full['test_status'] != 'ok')[0].tolist()
            data_dict['Channel Failure'] = data_full.loc[idx_ch_fail]['channel'].tolist()
            data_dict['Channel Failure Error'] = data_full['test_status'].loc[idx_ch_fail].values.tolist()
            

            # Get Validation Test date
            laser_id['val_date'] = sorted(report_data['Validation']['data'])[0][-33:-22].replace('_', ' ')

            # Laser Identification
            try:
                pi_data = np.load(
                    report_data['PxI Curve']['data'][0], allow_pickle=True)
                
                # Identification Data update
                laser_id['part_number'] = str(pi_data['header']['MODEL'].split(' ')[0])
                laser_id['serial_number'] = str(int(pi_data['header']['BOARD'].replace('\x00', '')))
                laser_id['gb_serial'] = gb_name.replace('_', ' ')
                laser_id['hw_serial'] = str(int(pi_data['header']['BOARD'].replace('\x00', '')))
                laser_id['hw_version'] = str(pi_data['header']['HW_VERSION'].replace('HW ', ''))
                laser_id['fw_version'] = pi_data['header']['FW_VERSION'][12:17]
            except:
                # Identification Data update
                laser_id['part_number'] = ''
                laser_id['serial_number'] = ''
                laser_id['gb_serial'] = ''
                laser_id['hw_serial'] = ''
                laser_id['hw_version'] = ''
                laser_id['fw_version'] = ''
                
            proc_obj.identification_data_capture(data_dict, laser_id)
            proc_obj.check_params_data_capture(data_dict)
            proc_obj.validation_data_capture(data_dict)
            proc_obj.phase_scan_data_capture(data_dict, config_dict)
            proc_obj.phase_margin_data_capture(data_dict)
            proc_obj.temperature_ramp_capture_data(data_dict)
            
            faults = proc_obj.failure_verification(data_dict, config_dict)
            data_dict['Faults'] = faults
            
            if len(data_dict['Faults']) != 0:
                data_dict['Status'] = 'NOT PASS'
            else:
                data_dict['Status'] = 'PASS'
        
            dataframe1 = pd.json_normalize(data_dict)
        
            list_data.append(dataframe1)

        # Dataframe Assignment
        if len(list_data) != 0:
            dataframe = pd.concat(list_data)

    try:
        dataframe = dataframe.reset_index(drop = True)
    except:
        pass
    
    #Power Consumption for DUAL and SINGLE
    if board_type == 'Dual':
        pc_total_hot = dataframe.iloc[0]['Power Consumption.Hot'] + dataframe.iloc[1]['Power Consumption.Hot'] + 1.3
        pc_total_cold = dataframe.iloc[0]['Power Consumption.Cold'] + dataframe.iloc[1]['Power Consumption.Cold'] + 1.3
        dataframe['Power Consumption.Cold'] = ['', pc_total_cold]
        dataframe['Power Consumption.Hot'] = ['', pc_total_hot]
    else:
        pc_total_hot = dataframe.iloc[0]['Power Consumption.Hot'] + 1.3
        pc_total_cold = dataframe.iloc[0]['Power Consumption.Cold'] + 1.3
        dataframe['Power Consumption.Hot'] = pc_total_hot
        dataframe['Power Consumption.Cold'] = pc_total_cold


    json_dict = copy.deepcopy(util.json_dict)
    
    for index in dataframe.index:
        json_dict['laser_type'] = dataframe.iloc[index]['ID'].split(' ')[0]
        json_dict['board_number'] = dataframe.iloc[index]['ID'].split(' ')[1]
        json_dict['goldbox_number'] = dataframe.iloc[index]['GB_SN']
        json_dict['validation_date'] = datetime.datetime.strptime(laser_id['val_date'], '%d %b %Y')
        json_dict['insertion_date'] = datetime.datetime.utcnow()
        json_dict['FW_version'] = dataframe.iloc[index]['FW Version']
        json_dict['check_params'] = dataframe.iloc[index]['Check Params']
        
        for i in range(0, len(dataframe.iloc[index]['Phase Scan'])):
            json_dict['phase_scan']['peak_' + str(len(dataframe.iloc[index]['Phase Scan'])-i)] = dataframe.iloc[index]['Phase Scan'][i]
                        
        json_dict['PVTC_check'] = dataframe.iloc[index]['PVT Check']
        json_dict["status"] = dataframe.iloc[index]['Status']
        
        json_dict['faults'] = dataframe.iloc[index]['Faults']
            
        #Output Optical Power
        json_dict['output_optical_power']['cold'] = dataframe.iloc[index]['Output Optical Power.Cold']
        json_dict['output_optical_power']['hot'] = dataframe.iloc[index]['Output Optical Power.Hot']
        
        #Output Power Accuracy
        json_dict['output_power_acc']['cold'] = dataframe.iloc[index]['Output Power Accuracy.Cold']
        json_dict['output_power_acc']['hot'] = dataframe.iloc[index]['Output Power Accuracy.Hot']

        #Frequency Accuracy
        json_dict['frequency_acc']['cold'] = dataframe.iloc[index]['Frequency Accuracy.Cold']
        json_dict['frequency_acc']['hot'] = dataframe.iloc[index]['Frequency Accuracy.Hot']

        #Tuning Time
        json_dict['tuning_time']['cold'] = dataframe.iloc[index]['Tuning Time.Cold']
        json_dict['tuning_time']['hot'] = dataframe.iloc[index]['Tuning Time.Hot']

        #Power Consumption
        if 'DUAL' in json_dict['laser_type'] and 'Slave' in json_dict['goldbox_number']:
            json_dict['power_consumption']['cold'] = dataframe.iloc[1]['Power Consumption.Cold']
            json_dict['power_consumption']['hot'] = dataframe.iloc[1]['Power Consumption.Hot']
        elif 'DUAL' in json_dict['laser_type'] and 'Master' in json_dict['goldbox_number']:
            json_dict['power_consumption']['cold'] = None
            json_dict['power_consumption']['hot'] = None
        else:
            json_dict['power_consumption']['hot'] = dataframe['Power Consumption.Hot'][0]
            json_dict['power_consumption']['cold'] = dataframe['Power Consumption.Cold'][0]
            
        #Phase Margin
        json_dict['phase_margin'] = dataframe.iloc[index]['Phase Margin']
        
        #Channel Failure
        fail_ch_dict = {}
        if len(dataframe.iloc[index]['Channel Failure']) != 0:
            for ch in range(0, len(dataframe.iloc[index]['Channel Failure'])):
                fail_ch_dict.update({str(dataframe.iloc[index]['Channel Failure'][ch]): dataframe.iloc[index]['Channel Failure Error'][ch]})
        else:
            fail_ch_dict = None
            
        json_dict['channel_failure'] = fail_ch_dict
        
        collection_obj.replace_one({"goldbox_number": {"$regex": json_dict['goldbox_number']}}, json_dict, upsert = True)

        json_dict.fromkeys(fault_dict, None)
        json_dict.pop('_id', None)

    return None
