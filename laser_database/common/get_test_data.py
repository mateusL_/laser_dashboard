# -*- coding: utf-8 -*-
"""
Copyright BrPhotonics Produtos Optoeletronicos S/A
@author:    Alexsandro Barros - alexsandro.junior@brphotonics.com
"""
# Python libraries
import os
import glob
import pickle


def goldbox_file(brp_path, report_path, laser_list):
    """
    Generate pickle dictionary with most recent goldbox test data paths
    Using pattern created by BrPhotonics hardware files

    Parameters
    ----------
    brp_path : STRING
        Path location of the tests folders.
    report_path : STRING
        Save path location where reports will be saved.
    laser_list : LIST
        List of goldbox serial numbers

    Returns
    -------
    filename_list : LIST OF STRINGS
        All pickle dictionary files paths.

    """
    filename_list = list()
    for laser in laser_list:
        
        # Initialize dict
        goldbox_dict = dict()
        
        # Goldbox Units PreTest Datapath    
        gb_name = 'Nano_GB_'+str(laser)
        gb_path = brp_path+'\\Goldbox Tests\\'+gb_name
        sealing_paths = glob.glob(gb_path+'/*')
        print(f'Goldbox: {gb_name}')
        
        for sealing_path in sealing_paths:
            if sealing_path.endswith('ini'):
                pass
            elif sealing_path.endswith('pkl'):
                pass
            else:
                # Goldbox Units Sealing Datapath
                sealing_name = sealing_path.replace(gb_path+'\\','')
                test_paths = glob.glob(sealing_path+'/*')
                
                # Initialize dict
                goldbox_dict[sealing_name] = dict()
                
                for test_path in test_paths:
                    if test_path.endswith('ini'):
                        pass
                    elif test_path.endswith('json'):
                        goldbox_dict[sealing_name]['Calibration File'] = test_path
                    else:
                        # Getting test name
                        test_name = test_path.replace(sealing_path+'\\','')
                        
                        # Initialize dict
                        goldbox_dict[sealing_name][test_name] = dict()
                        goldbox_dict[sealing_name][test_name]['data'] = list()
                        goldbox_dict[sealing_name][test_name]['images'] = list()
                        
                        # Get most recent execution of that test
                        recent_path = max([os.path.join(
                            test_path,d) for d in os.listdir(
                                test_path) if os.path.isdir(
                                    test_path+'\\'+d)], 
                                  key=os.path.getmtime)
                        
                        # List of files paths
                        files = glob.glob(recent_path+'/*')
                        
                        for file in files:
                            # List of all images paths
                            if file.endswith('png'):
                                goldbox_dict[sealing_name][test_name]['images'].append(file)
                            
                            # List of all data paths
                            elif file.endswith('pkl'):
                                goldbox_dict[sealing_name][test_name]['data'].append(file)
                            else:
                                pass
                            
        filepath = report_path +'\\Goldbox Reports\\'+gb_name
        if not os.path.exists(filepath):
                os.makedirs(filepath)
                
        filename = filepath+f'\\goldbox_path_dict.pkl'
        pickle.dump(goldbox_dict, open(filename, 'wb'))
        filename_list.append(filename)
    
    return filename_list


def test_file(brp_path, report_path, laser_dict):
    """
    Generate pickle dictionary with all most recent test data and images paths
    Using pattern created by BrPhotonics test files
    Keys: Goldbox Serial Numbers and Hardware Serial Number

    Parameters
    ----------
    brp_path : STRING
        Path location of the tests folders.
    report_path : STRING
        Save path location where reports will be saved.
    laser_dict : DICTIONARY
        KEY: INT - Hardware serial number:
        VALUE: STR - Hardware model (Dual, Single, etc.) 

    Returns
    -------
    filename_list : LIST OF STRINGS
        All pickle dictionary files paths.

    """
    filename_list = list()
    for laser_num, board_type in laser_dict.items():
        # Creating Unit Report Path
        hw_name = board_type + '_' + str(laser_num)
        print(f'Serial Number: {hw_name}')
        laser_path = report_path+'\\'+ hw_name
        if not os.path.exists(laser_path):
                os.makedirs(laser_path)
        
        # Unit Test Datapath      
        sn_path = brp_path+'\\Test and Calibration\\'+ hw_name
        
        # Goldbox Units Test Datapath
        gb_paths = glob.glob(sn_path+'/*')
        
        # Initialize Dict
        test_dict = dict()
        
        for gb_path in gb_paths:
            if gb_path.endswith(('ini','txt','pkl','log','data')):
                pass
            else:
                gb_name = gb_path.replace(sn_path+'\\','')
                test_dict[gb_name] = dict()
                print(f'GoldBox: {gb_name}')
                
                # Check Parameters Flag
                check_param = any([path.endswith('log') for path in gb_paths])
                if check_param: test_dict[gb_name]['Check Parameter'] = 'Pass'
                else: test_dict[gb_name]['Check Parameter'] = 'Fail'
                
                for test_path in glob.glob(gb_path+'/*'):
                    if test_path.endswith('ini'):
                        pass
                    
                    elif 'lock_setpoint' in test_path:
                        test_dict[gb_name]['Lock Setpoint'] = test_path
                    
                    # Calibration file datapath
                    elif test_path.endswith('json'):
                        test_dict[gb_name]['Calibration File'] = test_path
                    
                    # LUT datapath
                    elif test_path.endswith('LUT'):
                        # List with LUT Tests paths
                        lut_tests = glob.glob(test_path+'/*')
                        
                        for lut_test in lut_tests:
                            # Ignore desktop.ini file
                            if lut_test.endswith('ini'):
                                pass
                            else:
                                # Get test name
                                test_name = lut_test.replace(test_path+'\\','')
                                
                                # Initialize dict
                                test_dict[gb_name][test_name] = dict()
                                test_dict[gb_name][test_name]['images'] = list()
                                test_dict[gb_name][test_name]['data'] = list()
                                
                                # Get most recent execution of that test
                                recent_path = max([os.path.join(
                                    lut_test,d) for d in os.listdir(
                                        lut_test) if os.path.isdir(
                                            lut_test+'\\'+d)], 
                                          key=os.path.getmtime)
                                
                                # List of files paths
                                files = glob.glob(recent_path+'/*')
                                
                                for file in files:
                                    # List of all images paths
                                    if file.endswith('png'):
                                        test_dict[gb_name][
                                            test_name]['images'].append(file)
                                    
                                    # List of all data paths
                                    elif not file.endswith('ini'):
                                        test_dict[gb_name][
                                            test_name]['data'].append(file)                    
                    
                    # Validation datapath
                    elif test_path.endswith('freq_val'):
                        val_paths = glob.glob(test_path+'\\all_comp\\*')
                        
                        # Initialize dict
                        test_dict[gb_name]['Validation'] = dict()
                        test_dict[gb_name]['Validation']['data'] = list()
                        test_dict[gb_name]['Validation']['images'] = list()
                        val_data_path = list()
                        val_fig_path = list()
                        
                        for val_path in val_paths:
                            if val_path.endswith('ini'):
                                pass
                            elif val_path.endswith('figs'):
                                for fig_paths in glob.glob(val_path+'/*'):
                                    if fig_paths.endswith('ini'):
                                        pass
                                    else:
                                        val_fig_path.append(fig_paths)
                                for val_fig in glob.glob(max(
                                        val_fig_path,
                                        key=os.path.getmtime)+'/*.*'):
                                    if val_fig.endswith('ini'):
                                        pass
                                    else:
                                        # List of all images paths
                                        test_dict[gb_name]['Validation'][
                                            'images'].append(val_fig)
                            # Data folders
                            else:
                                val_data_path.append(val_path)
                        for data in glob.glob(max(
                                val_data_path, 
                                key=os.path.getmtime)+'/*.*'):
                            if data.endswith('ini'):
                                pass
                            else:
                                test_dict[gb_name]['Validation']['data'].append(data)
                                
                    # Remaining test datapath
                    else:
                        test_name = test_path.replace(gb_path+'\\','')
                        
                        # Initialize dict and lists
                        test_dict[gb_name][test_name] = dict()
                        test_dict[gb_name][test_name]['data'] = list()
                        test_dict[gb_name][test_name]['images'] = list()
                        
                        # Get most recent execution of that test
                        try:
                            recent_path = max([os.path.join(
                                test_path,d) for d in os.listdir(
                                    test_path) if os.path.isdir(test_path+'\\'+d)], 
                                          key=os.path.getmtime)
                        except:pass
                                
                        # List of files paths
                        files = glob.glob(recent_path+'/*')
                        
                        for file in files:
                            # List of all images paths
                            if file.endswith('png'):
                                test_dict[gb_name][
                                    test_name]['images'].append(file)
                                                        
                            # List of all data paths
                            elif not file.endswith('ini'):
                                test_dict[gb_name][
                                    test_name]['data'].append(file)
                            
        filename = laser_path+'\\test_path_dict.pkl'
        pickle.dump(test_dict, open(filename, 'wb'))
        filename_list.append(filename)
    
    return filename_list

if __name__ == '__main__':
    goldbox_list = [150]
    
    laser_dict = {
            5: 'Dual',
            83: 'Dual',
            80: 'Single_Slave'
    }

    brp_path = r'G:\Meu Drive\Dual Nano-ITLA\BrP'
    report_path = brp_path+'\\Test Reports'
    
    goldbox_list = goldbox_file(brp_path, report_path, goldbox_list)
    
    test_list = test_file(brp_path, report_path, laser_dict)