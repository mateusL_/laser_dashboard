"""
Copyright BrPhotonics Produtos Optoeletronicos S/A
@author:    
    Alexsandro Barros - alexsandro.junior@brphotonics.com
    Felipe Santos - felipe.santos@brphotonics.com
    Mateus Silva - mateus.silva@idea-ip.com
"""

# Report functions
from laser_database.common import get_test_data
from laser_database.common import quality_control_report
import time
from laser_database.process_data.yaml_parser import yaml_parser
from laser_database.process_data.connections import MongoConnect
import tempfile

class QualityDashboard(MongoConnect):
    def __init__(self, tests_folder, yaml_path):
        super().__init__()
        
        self.tests_folder = tests_folder
        self.yaml_path = yaml_path
        
        conn_obj = MongoConnect()
        self.db = conn_obj.database_connect()
        
    def run_dashboard(self, laser_type, laser_serial, verbose = True):
    
        # Create empty dict
        laser_dict = dict()     
        
        #Identifying LASER Type
        if laser_type == 'Single':
            laser_dict.update({laser_serial: 'Single'})
            collection_obj = self.db['uitla_single']
        elif laser_type == 'Dual':
            laser_dict.update({laser_serial: 'Dual'})
            collection_obj = self.db['uitla_dual']
        elif laser_type == 'QSFP':
            laser_dict.update({laser_serial: 'QSFP'})
            collection_obj = self.db['nitla_qsfp']
        elif laser_type == 'OSFP':
            laser_dict.update({laser_serial: 'OSFP'})
            collection_obj = self.db['nitla_osfp']
        else:
            return
        
        # Report Flags
        test_file_flag = True
        quality_control_report_flag = True
        
        #Temporary Directory
        temp_dir = tempfile.TemporaryDirectory()
            
        if test_file_flag:
            
            test_list = get_test_data.test_file(self.tests_folder, temp_dir.name, laser_dict)
                            
        if quality_control_report_flag:
            
            test_operator = 'Mateus Silva'
                        
            config_dict = yaml_parser(self.yaml_path).open_yaml()
            result = quality_control_report.quality_control_report(temp_dir.name, test_operator, laser_dict, config_dict, collection_obj = collection_obj)
            
            if result == None:
                print("LASER inserted on database sucessfuly")
                time.sleep(0.2)
                print(chr(27) + "[2J")
            else:
                print('Failure generating data report')
    
        temp_dir.cleanup()
        
            
