# -*- coding: utf-8 -*-

"""
Created on Wed Mar 30 16:33:46 2022

@author: mateus.silva
"""

from laser_database.common import report_unit

test_folder = r'G:\.shortcut-targets-by-id\1hEjfj6xr_TL7LBHky4q8hkBH9vULMO6w\Nano-ITLA'


Dual = [1408, 1584]
Single = []

report_object = report_unit.QualityDashboard(tests_folder=test_folder, yaml_path='quality_config.yaml')

for single in Single[:]:
    report_object.run_dashboard('Single', single)
    
for dual in Dual[:]:
    report_object.run_dashboard('Dual', dual)