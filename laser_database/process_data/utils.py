# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 11:25:24 2022

@author: mateus.silva
"""

import numpy as np
import idea_types.variables as var
from scipy.signal import find_peaks
from scipy.optimize import curve_fit
import pandas as pd

info_dict = {'ID': list(), 'GB_SN': list(), 
             'FW Version': list(), 
             'Status': list(), 
             'Faults': list(), 
             'Check Params': list(), 
             'Phase Scan': list(), 
             'Power Accuracy Ramp': list(), 
             'Frequency Ramp': list(), 
             'PVT Check': list(),
             'Phase Margin': list(),
             'Channel Failure': list(), 
             'Channel Failure Error': list(),
             'Output Optical Power': {'Hot': None, 'Cold': None}, 
             'Output Power Accuracy': {'Hot': None, 'Cold': None},  
             'Frequency Accuracy':{'Hot': None, 'Cold': None}, 
             'Power Consumption': {'Hot': None, 'Cold': None}, 
             'Tuning Time': {'Hot': None, 'Cold': None}}

faults_dict = {'Phase Scan': None, 'Output Power Accuracy': None, 'Power Accuracy Ramp': None,
              'Power Consumption': None, 'Tuning Time': None, 'Check Params': None, 
              'Frequency Accuracy': None, 'Frequency Ramp': None, 'Channel Failure': None,
              'Phase Margin': None}   

laser_id = {'test_location': 'BrP / Brazil', 'pwr_acc': '0', 'val_freq_err': '0',
            'pwr_cons': '0', 'laser_status': '', 'parameter': None}

json_dict = {
    "lot": None,
    "insertion_date": None,
    "GB_packaging": None,
    "validation_date": None,
    "laser_type": None,
    "board_number": None,
    "goldbox_number": None,
    "FW_version": None,
    "status": None,
    "faults": None,
    "check_params": None,
    "PVTC_check": None,
    "phase_margin": None,
    "phase_scan": {
        "peak_1": None,
        "peak_2": None},
    "output_power_acc": {
        "hot": None,
        "cold": None},
    "frequency_acc": {
        "hot": None,
        "cold": None},
    "power_consumption": {
        "hot": None,
        "cold": None},
    "tuning_time": {
        "hot": None,
        "cold": None},
    "output_optical_power": {
        "hot": None,
        "cold": None},
    "channel_failure": None
}

def lin_function(x, a, b):
    return a*x + b

def concat(words):
    st = ''
    if len(words) == 0:
        return st
    else:
        for err in words:
            st += str(err) + '/'
        return st[:-1]
    
def phase_margin(report_dict):
    
    calib_data = np.load(report_dict['Calibrate Dither Amplitude and Phase']['data'][0], allow_pickle=True)
    ps_T_points = calib_data['header'][var.INPUT_PARAMS]['ps_Tpoints']
    pd_mW = calib_data['data'][var.PD_POWER_mW][-ps_T_points:]
    ps_T = calib_data['data'][var.PHASE_TEMP][-ps_T_points:]
    sync_vector = np.array(list(reversed(calib_data['data'][var.DITHER_SYNC][-ps_T_points:])))
    if 'lock_setpoint' in calib_data['result']:
        setpoint = calib_data['result']['lock_setpoint']
    elif 'Lock Setpoint' in report_dict.keys():
        setpoint = np.load(report_dict['Lock Setpoint'], allow_pickle=True)['lock_setpoint']
    else:
        setpoint = None
    
        
    norm_pd_pwr = np.array(list(reversed(pd_mW / np.max(pd_mW))))
    ps_T = np.array(list(reversed(ps_T)))        
    
    peak_idx, _ = find_peaks(norm_pd_pwr)
    valley_idx, _ = find_peaks(-norm_pd_pwr)

    p1 = ps_T[valley_idx[0]+1]
    p2 = ps_T[peak_idx[0]] if valley_idx[0] < peak_idx[0] else ps_T[peak_idx[1]]
    p3 = ps_T[valley_idx[1]]
    try:
        if setpoint != None:
            setpoint_idx = np.where(sync_vector[valley_idx[0]+1:valley_idx[1]] == setpoint)[0][0]+valley_idx[0]+1
            p2 = ps_T[setpoint_idx]

        # phase margin calculation
        pm = ((p2-p1)/(p3-p1))
    except:
        a, b =  curve_fit(lin_function, ps_T[valley_idx[0]+1:valley_idx[1]], sync_vector[valley_idx[0]+1:valley_idx[1]])
        p2 = (setpoint - a[1])/a[0]
        pm = ((p2-p1)/(p3-p1))
    return pm
    
    
        
    