# -*- coding: utf-8 -*-
"""
Created on Sat Jan 15 23:41:08 2022

@author: mateu

"""

import yaml

class yaml_parser(object):
    def __init__(self, config_path):
        self.config_path = config_path

    def open_yaml(self):
        with open(self.config_path, "r") as stream:
            try:
                self.config_dict = yaml.safe_load(stream)
                print('Configuration loaded')
            except yaml.YAMLError as exc:
                print(exc)
            for key in self.config_dict['laser units'].keys():
                print(key)
                if self.config_dict['laser units'][key] is None:
                    self.config_dict['laser units'][key] = []
        return self.config_dict
    