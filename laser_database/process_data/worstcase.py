# -*- coding: utf-8 -*-
"""
Created on Wed May 25 09:10:28 2022

@author: mateus.silva
"""
import pandas as pd
import numpy as np
import idea_types.variables as var


class WorstCase():
    def __init__(self, df_full):
        self.df_full = df_full
        self.hot_cold_separation()
        
    def hot_cold_separation(self):
        self.hot_df = self.df_full[self.df_full[var.GOLDBOX_TEMP] > 0]
        self.cold_df = self.df_full[self.df_full[var.GOLDBOX_TEMP] < 0]
        
    def optical_power(self, hot_or_cold):
        if hot_or_cold == 'cold':
            last_values = self.__last_value(self.cold_df, var.PM_PWR_dBm)
        else:
            last_values = self.__last_value(self.hot_df, var.PM_PWR_dBm)
        wc_value = min(last_values)
        return float(wc_value)
    
    def optical_power_acc(self, hot_or_cold):
        if hot_or_cold == 'cold':
            last_values = self.__last_value(self.cold_df, var.PM_PWR_dBm)
        else:
            last_values = self.__last_value(self.hot_df, var.PM_PWR_dBm)
        tgt_power = self.df_full[var.TARGET_POWER][0]
        pwr_acc = np.array(last_values) - tgt_power
        
        wc_value = max(pwr_acc, key=abs)
        
        return float(wc_value)
    
    def frequency_acc(self, hot_or_cold):
        if hot_or_cold == 'cold':
            last_values = self.__last_value(self.cold_df, 'freq_wm')
        else:
            last_values = self.__last_value(self.hot_df, 'freq_wm')
        tgt_freq = var.ITU_FREQ
        freq_acc = np.array(last_values) - tgt_freq[:len(last_values)]       
        wc_value = max(freq_acc, key=abs)
        
        return float(wc_value)
    
    def tuning_time(self, hot_or_cold):
        if hot_or_cold == 'cold':
            tuning_time_val = self.cold_df['tuning_time'].tolist()
        else:
            tuning_time_val = self.hot_df['tuning_time'].tolist()
        wc_value = max(tuning_time_val, key=abs)
            
        return float(wc_value)
    
    def power_consumption(self, hot_or_cold):
        if hot_or_cold == 'cold':
            pc_list = self.__power_consumption_corrected(self.cold_df)                  
        else:
            pc_list = self.__power_consumption_corrected(self.hot_df)     
            
        wc_value = max(pc_list)
        
        return float(wc_value)

    def __power_consumption_corrected(self, df):
        pc = list()
        for row in df['total_consumption'].index:
            pc.append(100 * (df[var.PHASE_CURRENT][row] ** 2)*1e-6 \
                + 100 * (df[var.ET1_CURRENT][row] ** 2)*1e-6 \
                    + 100 * (df[var.ET2_CURRENT][row] ** 2)*1e-6 \
                        + df[var.GAINCHIP_MONITOR_VOLT][row] * df['gc_imean'][row]*1e-3 \
                            + np.abs(df[var.TEC_CURRENT][row] * df[var.TEC_VOLT][row]))
        
        return pc
        
    
    def __last_value(self, df, variable):
        last_value = [item[-1] if len(item) != 0 else 0 for item in df[variable]]
        return last_value