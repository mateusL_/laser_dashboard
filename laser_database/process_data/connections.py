# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 09:49:24 2022

@author: mateus.silva
"""

import pymongo
import certifi
from dataclasses import dataclass

@dataclass
class MongoConnect:
    user: str = 'production_bot'
    password: str = '8Lu8ZugFkR33asW8'
    str_conn: str = f"mongodb+srv://{user}:{password}@production.zgygk.mongodb.net/LASER_Production?retryWrites=true&w=majority"

    def database_connect(self):
        self.client_obj = pymongo.MongoClient(self.str_conn, tlsCAFile = certifi.where())
        db_obj = self.client_obj['LASER_Production']
                
        return db_obj
    
    def database_disconnect(self):
        self.client_obj.close()
        
        
if __name__ == '__main__':
    obj = MongoConnect().database_connect()