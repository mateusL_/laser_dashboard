# -*- coding: utf-8 -*-
"""
Created on Tue May 24 15:19:15 2022

@author: mateus.silva
"""

import numpy as np
import pandas as pd
import pprint as pp
import laser_database.process_data.utils as util
import laser_database.process_data.worstcase as wc
import idea_types.variables as var
from scipy.signal import find_peaks 


class ProcessTest():
    def __init__(self, report_dict):
        self.report_dict = report_dict
        #Mandatory: 1; Optional: 0
        self.test_needed = {'Validation': 1, 
                            'PxI Curve': 0,
                            'Temperature Ramp': 1,
                            'Phase_Scan': 1,
                            'Check Phase Lock Study': 0,
                            'Calibrate Dither Amplitude and Phase': 1}
        
    def verify_tests(self):
        self.unreach_mand_tests = list()
        self.unreach_optional_tests = list()
        for test in self.test_needed.keys():
            if test not in self.report_dict.keys() and self.test_needed[test] == 1:
                self.unreach_mand_tests.append(test)
            elif test not in self.report_dict.keys() and self.test_needed[test] == 0:
                self.unreach_optional_tests.append(test)
        print(70*'*')
        print('UNREACHBLE MANDATORY TESTS:')
        for i in self.unreach_mand_tests:
            print(f'         {i}')
        print(70*'*' + '\n')
        
        print(70*'*')
        print('UNREACHBLE OPTIONAL TESTS: ')
        for i in self.unreach_optional_tests:
            print(f'         {i}')
        print(70*'*' + '\n')
        
    def verify_channel(self):
        self.channel_failure_flag = False
        if 'Validation' not in self.unreach_mand_tests:
            for i in range(0, len(self.report_dict['Validation']['data'])):
                if '\\df_fail.pkl' in self.report_dict['Validation']['data'][i][-15:]:
                    val_data_f = np.load(self.report_dict['Validation']['data'][i],
                                         allow_pickle=True)
            for i in range(0, len(self.report_dict['Validation']['data'])):
                if '\\df_ok.pkl' in self.report_dict['Validation']['data'][i][-10:]:
                    val_data = np.load(self.report_dict['Validation']['data'][i],
                                       allow_pickle=True)
            if len(val_data_f[var.CHANNEL]) != 0:
                self.channel_failure_flag = True
                print('CHANNEL FAILURE FOUND')
                self.df_full = self.__concatenate_df(val_data, val_data_f)
            else:
                self.df_full = val_data
        else:
            raise Warning('Validation Test Unreachble')
        return self.df_full
    
    def validation_data_capture(self, dataframe_dict):
        
        wc_object = wc.WorstCase(self.df_full)
        
        #Optical Power Parameters            
        dataframe_dict['Output Optical Power']['Hot'] = wc_object.optical_power('hot')
        dataframe_dict['Output Optical Power']['Cold'] = wc_object.optical_power('cold')

        dataframe_dict['Output Power Accuracy']['Hot'] = wc_object.optical_power_acc('hot')
        dataframe_dict['Output Power Accuracy']['Cold'] = wc_object.optical_power_acc('cold')

        
        #Frequency Parameters
        dataframe_dict['Frequency Accuracy']['Hot'] = wc_object.frequency_acc('hot')
        dataframe_dict['Frequency Accuracy']['Cold'] = wc_object.frequency_acc('cold')

        #Tuning Time
        dataframe_dict['Tuning Time']['Hot'] = wc_object.tuning_time('hot')
        dataframe_dict['Tuning Time']['Cold'] = wc_object.tuning_time('cold')        
        
        #Power Consumption
        dataframe_dict['Power Consumption']['Hot'] = wc_object.power_consumption('hot') 
        dataframe_dict['Power Consumption']['Cold'] = wc_object.power_consumption('cold') 
        
    def identification_data_capture(self, dataframe_dict, laser_id):
        dataframe_dict['FW Version'] = laser_id['fw_version']
        dataframe_dict['GB_SN'] = laser_id['gb_serial']
        dataframe_dict['ID'] = laser_id['part_number'] + ' ' + laser_id['serial_number']
        
    def check_params_data_capture(self, dataframe_dict):
        dataframe_dict['Check Params'] = self.report_dict['Check Parameter']
        
    def phase_scan_data_capture(self, dataframe_dict, config_dict):
        approval_count = list()
        if 'Phase_Scan' in self.report_dict.keys():
            # Scanning for phase_scan.pkl
            for i in range(0, len(self.report_dict['Phase_Scan']['data'])):
                if '\\phase_scan.pkl' == self.report_dict['Phase_Scan']['data'][i][-15:]:
                    phase_data = np.load(self.report_dict['Phase_Scan']['data'][i], allow_pickle=True)
    
                    y_axis = phase_data['data'][var.PD_POWER_mW] / \
                        max(phase_data['data'][var.PD_POWER_mW])
                    peaks = find_peaks(y_axis)[0]
                    for peak in peaks[0:2]:
                        if peak <= 27:
                            if y_axis[peak+config_dict['criteria']['Phase Scan']['criteria']] >= (0.8*y_axis[peak]):
                                approval_count.append('Approved')
                            elif y_axis[peak+config_dict['criteria']['Phase Scan']['criteria']-1] >= (0.8*y_axis[peak]):
                                approval_count.append('Warning')
                            else:
                                approval_count.append('Reproved')
                        else:
                            approval_count.append('Reproved')
        else:
            approval_count.append('Missing Test')
            approval_count.append('Missing Test')
        if len(approval_count) > 0:
            dataframe_dict['Phase Scan'] = approval_count
        elif len(approval_count) == 0:
            dataframe_dict['Phase Scan'] = ['Missing Test', 'Missing Test']
            
    def phase_margin_data_capture(self, dataframe_dict):
        if 'Check Phase Lock Study' in self.report_dict.keys() or 'Calibrate Dither Amplitude and Phase' in self.report_dict.keys():
            try:
                pm = util.phase_margin(self.report_dict)
                dataframe_dict['Phase Margin'] = pm*100
            except:
                dataframe_dict['Phase Margin'] = 'Missing Test'
        else:
            dataframe_dict['Phase Margin'] = 'Missing Test'
            pm = None
            
    def temperature_ramp_capture_data(self, dataframe_dict):
        if 'Temperature Ramp' in self.report_dict.keys():
            for i in range(0, len(self.report_dict['Temperature Ramp']['data'])):
                if '#2.pkl' in self.report_dict['Temperature Ramp']['data'][i][-6:]:
                    ramp2_data = np.load(self.report_dict['Temperature Ramp']['data'][i], allow_pickle=True)
            delta_uniformity_power = max(ramp2_data['data'][var.TARGET_POWER]-ramp2_data['data'][var.PM_PWR_dBm])
            delta_freq = max(ramp2_data['result']['freq_err']) - min(ramp2_data['result']['freq_err'])
            dataframe_dict['Power Accuracy Ramp'] = delta_uniformity_power
            dataframe_dict['Frequency Ramp'] = delta_freq
            
            #PVT Check pickle not opening in this Python version, missing test is assumed by now.
            dataframe_dict['PVT Check'] = 'Missing Test'
        else:
            dataframe_dict['Power Accuracy Ramp'] = 'Missing Test'
            dataframe_dict['Frequency Ramp'] = 'Missing Test'
            
    def failure_verification(self, dataframe_dict, config_dict):
        fault_dict = list()
        for key in dataframe_dict.keys():
            if key == 'Channel Failure':
                if len(dataframe_dict[key]) != 0:
                    fault_dict.append(key)
            elif key == 'Check Params':
                if dataframe_dict[key] != 'Pass':
                    fault_dict.append(key)
            elif key == 'Frequency Accuracy' or key == 'Output Power Accuracy' or key == 'Tuning Time':
                for key_in in dataframe_dict[key]:
                    try:
                        if abs(dataframe_dict[key][key_in]) >= config_dict['criteria'][key]['criteria'] and key not in fault_dict:
                            fault_dict.append(key)
                    except:
                        pass
            elif key == 'Phase Margin':
                try:
                    if dataframe_dict[key] < config_dict['criteria'][key]['criteria']:
                        fault_dict.append(key)
                except:
                    pass
            elif key == 'Phase Scan':
                if 'Reproved' in dataframe_dict[key]:
                    fault_dict.append(key)
        return fault_dict

    def __last_value(self, variable):
        last_value = [item[-1] if len(item) != 0 else 0 for item in self.df_full[variable]]
        return last_value
   
    def __concatenate_df(self, df_ok, df_fail):
        concatenated_df = pd.concat([df_ok, df_fail])
        concatenated_df = concatenated_df.sort_index()
        return concatenated_df
        