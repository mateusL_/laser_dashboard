# -*- coding: utf-8 -*-
"""
Created on Wed Jun  1 09:20:25 2022

@author: mateus.silva
"""

from setuptools import setup, find_packages

setup(
    name="laser_database",
    version="0.1.0",
    packages=['laser_database.common', 'laser_database.process_data'],


    package_data={
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.txt', '*.rst'],
    },

    # metadata for upload
    author=["Mateus Silva"],
    author_email=["mateus.silva@idea-ip.com"],
    description="Database Laser MongoDB",
    license="MIT",
    keywords="laser database",
    install_requires=['numpy', 'scipy', 'pandas', 'pymongo', 'dnspython']
)
